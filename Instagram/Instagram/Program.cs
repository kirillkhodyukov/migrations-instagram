﻿using Instagram.models;
using System;
using System.Linq;

namespace Instagram
{
    class Program
    {

        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext()) 
            {
                if (!db.Users.Any())
                {
                    Users user1 = new Users()
                    {
                        Name = "Bill",
                        Surname = "Gates",
                        DateOfBirth = new DateTime(1955, 10, 28),
                        Login = "$BillGates$"
                    };

                    db.Users.Add(user1);
                    db.SaveChanges();
                }
            }
        }
    }
}
