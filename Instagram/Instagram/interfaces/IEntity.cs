﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Instagram.interfaces
{
    public interface IEntity
    {
        public int ID { get; set; }
    }
}
