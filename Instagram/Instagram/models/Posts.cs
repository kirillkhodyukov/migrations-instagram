﻿using Instagram.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Instagram.models
{
    public class Posts : IEntity
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Comment { get; set; }
    }
}
