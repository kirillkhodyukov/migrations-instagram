﻿using Instagram.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Instagram.models
{
    public class Users : IEntity
    {
        public int ID { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime CreateAccountDate { get
            {
                return DateTime.Now;
            } 
        }
    }
}
